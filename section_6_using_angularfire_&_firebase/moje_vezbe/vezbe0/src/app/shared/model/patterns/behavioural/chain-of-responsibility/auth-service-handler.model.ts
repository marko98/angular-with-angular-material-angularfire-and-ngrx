// models
import { BaseHandler } from './base-handler.model';
import { AuthService } from '../../../../service/auth.service';

// interfaces
import { Request, AUTH_SERVICE } from './request.interface';

export class AuthServiceHandler extends BaseHandler {

    constructor(private interestedSide: any){
        super();
    }

    handle = (request: Request) => {
        if (request !== undefined && request.getName() === AUTH_SERVICE) {
            this.interestedSide.isAuth = (<AuthService>request).isAuth();
        } else {
            if (this.next !== undefined) {
                this.next.handle(request);
            }
        }
    }
}