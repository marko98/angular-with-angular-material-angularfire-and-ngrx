// models
import { User } from '../../../user.model';
import { Factory } from './factory';

export class UserFactory extends Factory {
    create = () => {
        return new User();
    }
}