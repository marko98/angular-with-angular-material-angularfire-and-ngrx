// models
import { BaseHandler } from './base-handler.model';
import { LogoutDialog } from '../../../logout-dialog.model';

// interfaces
import { Request, DIALOG_LOGOUT } from './request.interface';

// services
import { AuthService } from '../../../../service/auth.service';

export class LogoutDialogHandler extends BaseHandler {

    constructor(private authService: AuthService){
        super();
    }

    handle = (request: Request) => {
        if (request !== undefined) { 
            if (request.getName() === DIALOG_LOGOUT) {
                if ((<LogoutDialog>request).result) {
                    this.authService.logout();
                }
            } else {
                if (this.next !== undefined) {
                    this.next.handle(request);
                }
            }
        }        
    }
}