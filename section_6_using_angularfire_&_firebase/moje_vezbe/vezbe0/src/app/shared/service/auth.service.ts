// models
import { User } from '../model/user.model';
import { AuthData } from '../model/auth-data.model';
import { UserFactory } from '../model/patterns/creational/factories/user-factory.model';
import { Observable } from '../model/patterns/behavioural/observer/observable.model';

// interfaces
import { Request, AUTH_SERVICE } from '../model/patterns/behavioural/chain-of-responsibility/request.interface';

export class AuthService extends Observable implements Request {
    private user: User;
    private userFactory: UserFactory;

    constructor(){
        super();
        this.userFactory = new UserFactory();
    }

    // ovde bi trebali da se salju zahtevi na server, za sada samo 'fake-ujemo'
    register = (authData: AuthData) => {
        this.user = this.userFactory.create();
        this.user.setEmail(authData.getEmail());
        this.user.setUserId(Math.round(Math.random() * 10000).toString());
        this.notify();
    }

    // ovde bi trebali da se salju zahtevi na server, za sada samo 'fake-ujemo'
    login = (authData: AuthData) => {
        this.user = this.userFactory.create();
        this.user.setEmail(authData.getEmail());
        this.user.setUserId(Math.round(Math.random() * 10000).toString());
        this.notify();
    }

    // ovde bi trebali da se salju zahtevi na server, za sada samo 'fake-ujemo'
    logout = () => {
        this.user = null;
        this.notify();
    }

    getUser = () => {
        // ne vracamo referencu jer bi neka druga komponenta onda
        // mogla da izemni user-a
        return this.user.clone();
    }

    isAuth = () => {
        return this.user != null;
    }
    
    notify = () => {
        this.observes.forEach(observer => {
            observer.update(this);
        });
    }

    getName = () => {
        return AUTH_SERVICE;
    }
}