import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

// interfaces
import { Observer } from '../../shared/model/patterns/behavioural/observer/observer.interface';

// models
import { AuthService } from '../../shared/service/auth.service';
import { AuthServiceHandler } from '../../shared/model/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';
import { LogoutDialogHandler } from '../../shared/model/patterns/behavioural/chain-of-responsibility/logout-dialog-handler.model';
import { LogoutDialog } from 'src/app/shared/model/logout-dialog.model';

// components
import { LogoutDialogComponent } from '../../shared/dialog/logout/logout-dialog.component';

@Component({
    selector: 'app-sidenav-list',
    templateUrl: './sidenav-list.component.html',
    styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit, OnDestroy, Observer {
    @Output() closeSidenav = new EventEmitter<void>();
    private authServiceHandler: AuthServiceHandler;
    private logoutDialogComponent: LogoutDialogComponent;
    private matDialogRef: MatDialogRef<LogoutDialogComponent>;
    public isAuth: boolean = false;

    constructor(private authService: AuthService,
                private matDialog: MatDialog){}

    onLogout = () => {
        this.matDialogRef = this.matDialog.open(LogoutDialogComponent, {});
        this.logoutDialogComponent = this.matDialogRef.componentInstance;
        this.logoutDialogComponent.setModel(new LogoutDialog("Marko"));
        this.logoutDialogComponent.attach(this);
        this.onCloseSidenav();
    }

    onCloseSidenav = () => {
        this.closeSidenav.emit();
    }

    update = (request?: any) => {
        if (request !== undefined){
            this.authServiceHandler.handle(request);
        }
    }

    ngOnInit(){
        this.authService.attach(this);
        this.authServiceHandler = new AuthServiceHandler(this);
        let logoutDialogHandler = new LogoutDialogHandler(this.authService);
        this.authServiceHandler.setNext(logoutDialogHandler);
        console.log("SidenavListComponent init");
    }

    ngOnDestroy(){
        this.authService.dettach(this);
        if (this.logoutDialogComponent !== undefined)
            this.logoutDialogComponent.dettach(this);
        console.log("SidenavListComponent destroyed");
    }
}