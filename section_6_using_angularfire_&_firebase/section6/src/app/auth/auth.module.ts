import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

// components
import { LoginComponent } from './login/login.component';
import { SignupComponent} from './signup/signup.component';

@NgModule({
    declarations: [
        LoginComponent,
        SignupComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        LoginComponent,
        SignupComponent
    ]
})
export class AuthModule{}