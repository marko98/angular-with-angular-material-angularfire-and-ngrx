import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// services
import { AuthService } from '../../shared/services/auth.service';

// factories
import { AuthDataFactory } from '../../shared/models/patterns/creational/factories/auth-data-factory';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    public loginForm: FormGroup;
    public showPassword: boolean = false;
    private authDataFactory: AuthDataFactory;

    constructor(private authService: AuthService){
        this.authDataFactory = new AuthDataFactory();
    }

    onSubmit = () => {
        let authData = this.authDataFactory.create();
        authData.setEmail(this.loginForm.value.email);
        authData.setPassword(this.loginForm.value.password);
        this.authService.login(authData);
        // console.log(this.loginForm);
    }

    onShowPassword = () => {
        this.showPassword = true;

        setTimeout(() => {
            this.showPassword = false;
        }, 1000);
    }

    ngOnInit(){
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email], []),
            password: new FormControl('', [Validators.required, Validators.minLength(6)], [])
        });
        console.log("LoginComponent init");
    }

    ngOnDestroy(){
        console.log("LoginComponent destroyed");
    }
}