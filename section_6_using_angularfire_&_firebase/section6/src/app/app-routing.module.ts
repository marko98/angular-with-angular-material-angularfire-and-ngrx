import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { TrainingComponent } from './training/training.component';
import { WelcomeComponent } from './welcome/welcome.component';

// models
import { AuthGuard } from './shared/models/auth.guard';

const routes: Routes = [
    { path: '', component: WelcomeComponent },
    { path: 'auth', children: [
        { path: 'login', component: LoginComponent },
        { path: 'signup', component: SignupComponent }
    ]},
    { path: 'trainings', component: TrainingComponent, canActivate: [AuthGuard] },
    { path: '**', redirectTo: '' }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule{}