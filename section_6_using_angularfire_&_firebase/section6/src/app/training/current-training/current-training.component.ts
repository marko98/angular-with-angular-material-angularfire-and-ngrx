import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

// components
import { StopTrainingDialogComponent } from '../../shared/dialogs/stop-training/stop-training-dialog.component';

// custom interfaces
import { Observer } from '../../shared/models/patterns/behavioural/observer/observer.interface';
import { StopTrainingDialogHandlerInterface } from '../../shared/interfaces/stop-training-dialog-handler.interface';

// services
import { TrainingService } from '../../shared/services/training.service';

// models
import { Exercise } from '../../shared/models/exercise.model';
import { BaseHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { StopTrainingDialogHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/stop-training-dialog-handler';

@Component({
    selector: 'app-current-training',
    templateUrl: './current-training.component.html',
    styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit, OnDestroy, Observer, StopTrainingDialogHandlerInterface{
    public progress: number = 0;
    public timer: number;
    public trainingInProgress: Exercise;

    private stopTrainingDialogComponent: StopTrainingDialogComponent;
    private handler: BaseHandler;

    // MatDialog je servis koji nam sluzi da bismo instancirali komponentu u run-time-u
    // wrap-uje nasu komponentu i ona dobija jos neke osobine (kao dekorater) 
    constructor(private dialog: MatDialog,
                private trainingService: TrainingService){}

    onStop = () => {
        clearInterval(this.timer);
        // prosledjujemo nasu komponentu koja mora biti unutar:
        // entryComponents: [
        //        StopTrainingDialogComponent
        //    ]
        // u modulu koji je 'export-uje'
        let dialogRef = this.dialog.open(StopTrainingDialogComponent, {
            width: '250px'
        });
        this.stopTrainingDialogComponent = (<StopTrainingDialogComponent>dialogRef.componentInstance);
        this.stopTrainingDialogComponent.stopTrainingDialogData.attach(this);

        this.stopTrainingDialogComponent.setModelData({name: "Marko", progress: this.progress});
    }

    update = (data?: any) => {
        this.handler.handle(data);
    }

    onStopTrainingDialogUpdate = () => {
        // sada znamo da se data iz metode update ticala StopTrainingData modela
        if (!(this.stopTrainingDialogComponent.stopTrainingDialogData.stopTraining)){
            this.startOrResumeTimer();
        } else {
            this.trainingService.cancelExercise(this.progress);
        }

        // console.log((<StopTrainingDialogComponent>this.dialogRef.componentInstance).stopTrainingDialogData.stopTraining);
    }

    private startOrResumeTimer = () => {
        let step = this.trainingService.getTrainingInProgress().getDuration() / 100 * 1000;

        this.timer = setInterval(() => {
            this.progress += 1;

            if (this.progress >= 100){
                clearInterval(this.timer);
                this.trainingService.completeExercise();
            }

        }, step);
    }

    ngOnInit(){
        this.handler = new StopTrainingDialogHandler(this);

        this.trainingInProgress = this.trainingService.getTrainingInProgress();
        this.startOrResumeTimer();
        console.log("CurrentTrainingComponent init");
    }

    ngOnDestroy(){
        if (this.stopTrainingDialogComponent)
            this.stopTrainingDialogComponent.stopTrainingDialogData.dettach(this);
        console.log("CurrentTrainingComponent destroyed");
    }

}