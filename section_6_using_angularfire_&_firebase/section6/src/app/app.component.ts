import { Component, OnInit, OnDestroy } from '@angular/core';

// services
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'section6';

  constructor(private authService: AuthService){}

  ngOnInit(){
    console.log("AppComponent init");
  }

  ngOnDestroy(){
      console.log("AppComponent destroyed");
  }
}
