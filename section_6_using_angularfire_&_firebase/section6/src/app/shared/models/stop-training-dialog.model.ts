import { Observable } from '../models/patterns/behavioural/observer/observable.model';

export class StopTrainingDialog extends Observable {
    public stopTraining: boolean;
    public name: string;
    public progress: number;

    notify = () => {
        this.observers.forEach(observer => {
            observer.update(this);
        });
    }
}