export declare interface Observer {
    update(data?: any): void;
}