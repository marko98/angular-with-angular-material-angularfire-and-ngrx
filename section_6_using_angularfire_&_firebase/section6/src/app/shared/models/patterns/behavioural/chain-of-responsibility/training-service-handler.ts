// models
import { BaseHandler } from './base-handler.model';
import { TrainingService } from '../../../../services/training.service';

// interfaces
import { Request, TRAINING_SERVICE } from './request.interface';
import { TrainingServiceHandlerInterface } from '../../../../interfaces/training-service-handler.interface';

export class TrainingServiceHandler extends BaseHandler {

    constructor(private interestedSide: TrainingServiceHandlerInterface){
        super();
    }

    handle = (request: Request) => {
        if (request) {
            let trainingService = <TrainingService>request;
            if(trainingService.getName() && trainingService.getName() === TRAINING_SERVICE){
                this.interestedSide.onTrainingServiceUpdate();
            } else {
                if (this.next)
                    this.next.handle(request);
            }
        }
    }
}