// models
import { BaseHandler } from './base-handler.model';
import { StopTrainingDialog } from '../../../stop-training-dialog.model';

// interfaces
import { Request } from './request.interface';
import { StopTrainingDialogHandlerInterface } from '../../../../interfaces/stop-training-dialog-handler.interface';

export class StopTrainingDialogHandler extends BaseHandler {
    constructor(private interestedSide: StopTrainingDialogHandlerInterface){
        super();
    }

    handle = (request: Request) => {
        if (request) {
            let stopTrainingDialog = <StopTrainingDialog><unknown>request;
            if (stopTrainingDialog){
                this.interestedSide.onStopTrainingDialogUpdate();
            } else {
                if (this.next)
                    this.next.handle(request);
            }
        }
    }
}