// models
import { BaseHandler } from './base-handler.model';

// interfaces
import { Request, AUTH_SERVICE } from './request.interface';
import { AuthServiceHandlerInterface } from '../../../../interfaces/auth-service-handler.interface';

// services
import { AuthService } from '../../../../services/auth.service';

export class AuthServiceHandler extends BaseHandler {
    constructor(private interestedSide: AuthServiceHandlerInterface){
        super();
    }

    handle = (request: Request) => {
        // proverava da li je authService razlicito od
        // null ili undefined
        if(request){
            let authService = <AuthService>request;
            if (authService.getName() && authService.getName() === AUTH_SERVICE){
                this.interestedSide.onAuthServiceUpdate();
            } else {
                if (this.next)
                    this.next.handle(request);
            }
        }
    }
}