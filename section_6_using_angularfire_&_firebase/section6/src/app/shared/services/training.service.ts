import { Injectable, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';

// operators
import { map } from 'rxjs/operators';

// models
import { Exercise } from '../models/exercise.model';
import { ExerciseFactory } from '../models/patterns/creational/factories/exercise-factory';
import { Observable } from '../models/patterns/behavioural/observer/observable.model';
import { BaseHandler } from '../models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { AuthServiceHandler } from '../models/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';

// interfaces
import { Request, TRAINING_SERVICE } from '../models/patterns/behavioural/chain-of-responsibility/request.interface';
import { Firestore } from '../interfaces/firestore.interface';
import { Observer } from '../models/patterns/behavioural/observer/observer.interface';
import { AuthServiceHandlerInterface } from '../interfaces/auth-service-handler.interface';

// services
import { AuthService } from './auth.service';

const FINISHED_EXERCISES = "finishedExercises";
const AVAILABLE_EXERCISES = "availableExercises";

@Injectable()
export class TrainingService extends Observable implements OnInit, Request, Observer, AuthServiceHandlerInterface {
    private availableExercises: Exercise[] = [];
    private exerciseFactory: ExerciseFactory;
    private trainingInProgress: Exercise;
    private completedOrCancelledExercises: Exercise[] = [];
    private handler: BaseHandler;
    private firebaseSubscriptions: Subscription[] = [];

    constructor(private angularFirestoreDB: AngularFirestore,
                private authService: AuthService) {
        super();
        this.exerciseFactory = new ExerciseFactory();
        this.setFetchAvailableExercisesSubscription();
        this.setFetchCompletedOrCancelledExercisesSubscription();

        this.handler = new AuthServiceHandler(this);
        this.authService.attach(this);
    }

    completeExercise = () => {
        this.trainingInProgress.setDate(new Date());
        this.trainingInProgress.setState("completed");

        this.addDataToDatabase(this.trainingInProgress, FINISHED_EXERCISES);
        this.trainingInProgress = null;
        this.notify();
    }

    cancelExercise = (progress: number) => {
        this.trainingInProgress.setDate(new Date());
        this.trainingInProgress.setState("cancelled");
        this.trainingInProgress.setDuration(this.trainingInProgress.getDuration() * (progress / 100));
        this.trainingInProgress.setCalories(this.trainingInProgress.getCalories() * (progress / 100));

        this.addDataToDatabase(this.trainingInProgress, FINISHED_EXERCISES);
        this.trainingInProgress = null;
        this.notify();
    }

    getCompletedOrCancelledExercises = () => {
        // vracamo kopiju
        return this.completedOrCancelledExercises.slice();
    }

    setFetchCompletedOrCancelledExercisesSubscription = () => {
        this.firebaseSubscriptions.push(
            this.angularFirestoreDB
                .collection(FINISHED_EXERCISES)
                .valueChanges() // <- listener
                .pipe(
                    map(result => {
                        return this.getExercisesFromArray(result);
                    })
                )
                .subscribe((exercises: Exercise[]) => {
                    this.completedOrCancelledExercises = exercises;
                    this.notify();
                })
        );
    }

    setTrainingInProgress = (id: string) => {
        // this.angularFirestoreDB.doc(AVAILABLE_EXERCISES + id).update({lastSelected: new Date()});
        let original = this.availableExercises.find(exercise => exercise.getId() === id);
        this.trainingInProgress = (<Exercise>original.clone());
        this.notify();
    }

    getTrainingInProgress = () => {
        if (this.trainingInProgress)
            return (<Exercise>this.trainingInProgress.clone());
        return undefined;
    }

    getAvailableExercises = () => {
        // vracamo samo kopije
        // return [ ...this.availableExercises ];
        return this.availableExercises.slice();
    }

    setFetchAvailableExercisesSubscription = () => {
        this.firebaseSubscriptions.push(
            this.angularFirestoreDB
                .collection(AVAILABLE_EXERCISES)
                // .valueChanges() // <- listener
                .snapshotChanges() // <- listener
                .pipe(
                    map(result => {
                        let resultArray = result.map(r => {
                            let data = r.payload.doc.data();
                            data["id"] = r.payload.doc.id;
                            return data;

                            // return {
                            //     id: r.payload.doc.id,
                            //     ...r.payload.doc.data()
                            // }
                        });
                        return this.getExercisesFromArray(resultArray);
                    })
                )
                .subscribe((exercises: Exercise[]) => {
                    this.populateAvailableExercises(exercises);
                })
        );
    }

    populateAvailableExercises = (exercisesData: Exercise[]) => {
        // let exercisesData = [
        //     { id: 'crunches', name: 'Crunches', duration: 30, calories: 8 },
        //     { id: 'touch-toes', name: 'Touch Toes', duration: 180, calories: 15 },
        //     { id: 'side-lunges', name: 'Side Lunges', duration: 120, calories: 18 },
        //     { id: 'burpees', name: 'Burpees', duration: 60, calories: 8 }
        // ];

        exercisesData.forEach(exerciseData => {
            let foundExercise = this.availableExercises.find(exercise => exercise.getId() === exerciseData.getId());
            if (foundExercise){
                foundExercise.setId(exerciseData.getId());
                foundExercise.setName(exerciseData.getName());
                foundExercise.setDuration(exerciseData.getDuration());
                foundExercise.setCalories(exerciseData.getCalories());
            } else {
                let exercise = this.exerciseFactory.create();
                exercise.setId(exerciseData.getId());
                exercise.setName(exerciseData.getName());
                exercise.setDuration(exerciseData.getDuration());
                exercise.setCalories(exerciseData.getCalories());
                this.availableExercises.push(exercise);
            }
        });
        this.notify();
    }

    getExercisesFromArray = (exercisesData: any[]): Exercise[] => {
        let exercises = [];
        exercisesData.forEach(exerciseData => {
            let exercise = this.exerciseFactory.create();
            if (exerciseData.id)
                exercise.setId(exerciseData.id);
            if (exerciseData.name)
                exercise.setName(exerciseData.name);
            if (exerciseData.duration)
                exercise.setDuration(exerciseData.duration);
            if (exerciseData.calories)
                exercise.setCalories(exerciseData.calories);
            if (exerciseData.date)
                exercise.setDate(new Date(exerciseData.date.seconds*1000));
            if (exerciseData.state)
                exercise.setState(exerciseData.state);
            exercises.push(exercise);
        });
        return exercises;
    }

    private addDataToDatabase = (document: Firestore, collectionName: string): void => {
        this.angularFirestoreDB
            .collection(collectionName)
            .add(document.getFields())
            .then(data => {
                // console.log(data);
            })
            .catch(err => {
                console.log(err);
            });
    }

    onAuthServiceUpdate = () => {
        if (!this.authService.getIsAuth()){
            this.firebaseSubscriptions.forEach(firebaseSubscription => {
                firebaseSubscription.unsubscribe();
            });
        }
    }

    update = (data?: any) => {
        if (data)
            this.handler.handle(data);
    }

    notify = () => {
        this.observers.forEach(observer => {
            observer.update(this);
        });
    }

    getName = () => {
        return TRAINING_SERVICE;
    }

    ngOnInit(){
        console.log("TrainingService init");
    }
}