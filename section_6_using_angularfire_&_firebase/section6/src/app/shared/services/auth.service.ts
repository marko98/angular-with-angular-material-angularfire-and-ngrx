import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

// interfaces
import { Request, AUTH_SERVICE } from '../models/patterns/behavioural/chain-of-responsibility/request.interface';

// models
import { Observable } from '../models/patterns/behavioural/observer/observable.model';
import { AuthData } from '../models/auth-data.model';

// ovaj service sluzi samo da 'fake-ujemo' serversku 
// stranu za sada
@Injectable()
export class AuthService extends Observable implements Request {
    private isAuth: boolean = false;

    constructor(private router: Router,
                private angularFireAuth: AngularFireAuth){
        super();
        this.autoLoginLogout();
    }

    registerUser = (authData: AuthData) => {
        this.angularFireAuth.
            auth.
            createUserWithEmailAndPassword(authData.getEmail(), authData.getPassword())
            .then(result => {
                // console.log(result);
            })
            .catch(err => {
                alert(err);
                console.log(err);
            });
    }

    private autoLoginLogout = () => {
        this.angularFireAuth
            .authState
            .subscribe(
                user => {
                    if (user) {
                        this.isAuth = true;
                        this.notify();
                        this.router.navigate(['trainings']);
                    } else {
                        this.isAuth = false;
                        this.notify();
                        this.router.navigate(['auth', 'login']);
                    }
                },
                err => {
                    console.log(err);
                }
            );
    }

    login = (authData: AuthData) => {
        this.angularFireAuth
            .auth
            .signInWithEmailAndPassword(authData.getEmail(), authData.getPassword())
            .then(result => {
                // console.log(result);
            })
            .catch(err => {
                alert(err);
                console.log(err);
            });
    }

    logout = () => {
        this.angularFireAuth.auth.signOut();
    }

    getIsAuth = () => {
        return this.isAuth;
    }

    notify = () => {
        this.observers.forEach(observer => {
            observer.update(this);
        });
    }

    getName = () => {
        return AUTH_SERVICE;
    }
}