export declare interface StopTrainingDialogHandlerInterface {
    onStopTrainingDialogUpdate(): void;
}