import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'section5';

  ngOnInit(){
    console.log("AppComponent init");
  }

  ngOnDestroy(){
      console.log("AppComponent destroyed");
  }
}
