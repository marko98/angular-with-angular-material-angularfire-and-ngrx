import { Component, OnInit, OnDestroy } from '@angular/core';

// models
import { BaseHandler } from '../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { TrainingServiceHandler } from '../shared/models/patterns/behavioural/chain-of-responsibility/training-service-handler';

// interfaces
import { Observer } from '../shared/models/patterns/behavioural/observer/observer.interface';
import { TrainingServiceHandlerInterface } from '../shared/interfaces/training-service-handler.interface';

// services
import { TrainingService } from '../shared/services/training.service';

@Component({
    selector: 'app-training',
    templateUrl: './training.component.html',
    styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit, OnDestroy, Observer, TrainingServiceHandlerInterface {
    public ongoingTraining: boolean = false;
    private handler: BaseHandler;

    constructor(private trainingService: TrainingService){}

    update = (data?: any) => {
        if (data)
            this.handler.handle(data);
    }

    onTrainingServiceUpdate = () => {
        // sada znamo da se data iz metode update ticala Training Service-a
        let exercise = this.trainingService.getTrainingInProgress();
        if (exercise) {
            this.ongoingTraining = true;
            return;
        }
        this.ongoingTraining = false;
    }
    
    ngOnInit(){
        this.trainingService.attach(this);
        this.handler = new TrainingServiceHandler(this);
        console.log("TrainingComponent init");
    }

    ngOnDestroy(){
        if (this.trainingService)
            this.trainingService.dettach(this);
        console.log("TrainingComponent destroyed");
    }
}