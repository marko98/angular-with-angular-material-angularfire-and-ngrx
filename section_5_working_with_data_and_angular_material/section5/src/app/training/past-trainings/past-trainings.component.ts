import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

// models
import { Exercise } from '../../shared/models/exercise.model';
import { BaseHandler } from 'src/app/shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { TrainingServiceHandler } from 'src/app/shared/models/patterns/behavioural/chain-of-responsibility/training-service-handler';

// services
import { TrainingService } from 'src/app/shared/services/training.service';

// interfaces
import { Observer } from 'src/app/shared/models/patterns/behavioural/observer/observer.interface';
import { TrainingServiceHandlerInterface } from 'src/app/shared/interfaces/training-service-handler.interface';

@Component({
    selector: 'app-past-trainings',
    templateUrl: './past-trainings.component.html',
    styleUrls: ['./past-trainings.component.css']
})
export class PastTrainingsComponent implements OnInit, AfterViewInit, OnDestroy, Observer, TrainingServiceHandlerInterface {
    @ViewChild('paginator', {static: false}) paginator: MatPaginator;
    // or
    // @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    
    // ovde se definise red kolona, tj on zavisi od redosleda ove liste
    public displayedColumns: string[] = ['date', 'name', 'duration', 'calories', 'state'];
    public dataSource = new MatTableDataSource<Exercise>();
    public pageSizeOptions: number[] = [2, 5, 10, 20];
    // public filter: string = "";

    private handler: BaseHandler;

    constructor(private trainingService: TrainingService){}

    update = (data?: any) => {
        if (data)
            this.handler.handle(data);
    }

    onTrainingServiceUpdate = () => {
        // sada znamo da se data iz metode update ticala Training Service-a
        this.dataSource.data = this.trainingService.getCompletedOrCancelledExercises().reverse();
    }

    onKeyUp = (filterValue: string) => {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    ngOnInit(){
        this.handler = new TrainingServiceHandler(this);
        this.trainingService.attach(this);
        this.dataSource.data = this.trainingService.getCompletedOrCancelledExercises().reverse();
        
        let i = this.dataSource.data.length;
        console.log("PastTrainingsComponent init");
    }

    ngAfterViewInit(){
        // buduci da ih 'hvatamo' sa tamplate-a on u trenutku kada se poziva ngOnInit
        // nije jos spremani zbog toga ne mozemo tada 'hvatati' elemente
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    ngOnDestroy(){
        if (this.trainingService)
            this.trainingService.dettach(this);
        console.log("PastTrainingsComponent destroyed");
    }

}