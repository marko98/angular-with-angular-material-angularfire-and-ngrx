import { Prototype } from './patterns/creational/prototype/prototype.interface';

export class User implements Prototype {
    private email: string;
    private userId: string;

    getEmail = () => {
        return this.email;
    }

    getUserId = () => {
        return this.userId;
    }

    setEmail = (email: string) => {
        this.email = email;
    }

    setUserId = (userId: string) => {
        this.userId = userId;
    }

    protected userPrototype = (user: User): void => {
        user.setEmail(this.email);
        user.setUserId(this.userId);
    }

    clone = (): Prototype => {
        let clone = new User();
        clone.setEmail(this.email);
        clone.setUserId(this.userId);
        return clone;
    }

}