// interfaces
import { Prototype } from './patterns/creational/prototype/prototype.interface';

export class Exercise implements Prototype {
    private id: string; 
    private name: string;
    private duration: number; 
    private calories: number;

    constructor(private date?: Date, 
        private state?: 'completed' | 'cancelled' | null){}

    getId = () => {
        return this.id;
    }

    getName = () => {
        return this.name;
    }

    getDuration = () => {
        return this.duration;
    }

    getCalories = () => {
        return this.calories;
    }

    getDate = () => {
        return this.date;
    }

    getState = () => {
        return this.state;
    }

    setId = (id: string) => {
        this.id = id;
    }

    setName = (name: string) => {
        this.name = name;
    }

    setDuration = (duration: number) => {
        this.duration = duration;
    }

    setCalories = (calories: number) => {
        this.calories = calories;
    }

    setDate = (date: Date) => {
        this.date = date;
    }

    setState = (state: 'completed' | 'cancelled' | null) => {
        this.state = state;
    }

    protected exercisePrototype = (exercise: Exercise): void => {
        exercise.setId(this.getId());
        exercise.setName(this.getName());
        exercise.setDuration(this.getDuration());
        exercise.setCalories(this.getCalories());
        exercise.setDate(this.getDate());
        exercise.setState(this.getState());
    }

    clone = (): Prototype => {
        let clone = new Exercise();
        clone.setId(this.getId());
        clone.setName(this.getName());
        clone.setDuration(this.getDuration());
        clone.setCalories(this.getCalories());
        clone.setDate(this.getDate());
        clone.setState(this.getState());
        return clone;
    }
}