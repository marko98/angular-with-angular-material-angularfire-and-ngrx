// models
import { Exercise } from '../../../exercise.model';

// interfaces
import { Factory } from './factory.interface';

export class ExerciseFactory implements Factory {
    create = () => {
        return new Exercise();
    }
}