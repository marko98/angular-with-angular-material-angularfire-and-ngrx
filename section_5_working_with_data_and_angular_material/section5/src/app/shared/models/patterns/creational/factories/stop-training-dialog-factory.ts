// models
import { StopTrainingDialog } from '../../../stop-training-dialog.model';

// interfaces
import { Factory } from './factory.interface';

export class StopTrainingDialogFactory implements Factory {
    create = () => {
        return new StopTrainingDialog();
    }
}