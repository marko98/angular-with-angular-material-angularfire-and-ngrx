// models
import { User } from '../../../user.model';

// interfaces
import { Factory } from './factory.interface';

export class UserFactory implements Factory {
    create = () => {
        return new User();
    }
}