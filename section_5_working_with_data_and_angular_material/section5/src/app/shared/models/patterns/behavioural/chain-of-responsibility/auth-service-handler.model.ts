// models
import { BaseHandler } from './base-handler.model';
import { AUTH_SERVICE } from './request.interface';

// services
import { AuthService } from '../../../../services/auth.service';

export class AuthServiceHandler extends BaseHandler {
    constructor(private interestedSide: any){
        super();
    }

    handle = (request: any) => {
        // proverava da li je authService razlicito od
        // null ili undefined
        if(request){
            let authService = <AuthService>request;
            if (authService.getName() && authService.getName() === AUTH_SERVICE){
                this.interestedSide.isAuth = authService.isAuth();
            } else {
                if (this.next)
                    this.next.handle(request);
            }
        }
    }
}