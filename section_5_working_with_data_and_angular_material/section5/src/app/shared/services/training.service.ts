import { Injectable } from '@angular/core';

// models
import { Exercise } from '../models/exercise.model';
import { ExerciseFactory } from '../models/patterns/creational/factories/exercise-factory';
import { Observable } from '../models/patterns/behavioural/observer/observable.model';

// interfaces
import { Request, TRAINING_SERVICE } from '../models/patterns/behavioural/chain-of-responsibility/request.interface';

@Injectable()
export class TrainingService extends Observable implements Request {
    private availableExercises: Exercise[] = [];
    private exerciseFactory: ExerciseFactory;
    private trainingInProgress: Exercise;
    private completedOrCancelledExercises: Exercise[] = [];

    constructor() {
        super();
        this.exerciseFactory = new ExerciseFactory();
        this.populateAvailableExercises();
    }

    completeExercise = () => {
        this.trainingInProgress.setDate(new Date());
        this.trainingInProgress.setState("completed");

        this.completedOrCancelledExercises.push(this.trainingInProgress);
        this.trainingInProgress = null;
        this.notify();
    }

    cancelExercise = (progress: number) => {
        this.trainingInProgress.setDate(new Date());
        this.trainingInProgress.setState("cancelled");
        this.trainingInProgress.setDuration(this.trainingInProgress.getDuration() * (progress / 100));
        this.trainingInProgress.setCalories(this.trainingInProgress.getCalories() * (progress / 100));

        this.completedOrCancelledExercises.push(this.trainingInProgress);
        this.trainingInProgress = null;
        this.notify();
    }

    getCompletedOrCancelledExercises = () => {
        return this.completedOrCancelledExercises.slice();
    }

    setTrainingInProgress = (id: string) => {
        let original = this.availableExercises.find(exercise => exercise.getId() === id);
        this.trainingInProgress = (<Exercise>original.clone());
        this.notify();
    }

    getTrainingInProgress = () => {
        if (this.trainingInProgress)
            return (<Exercise>this.trainingInProgress.clone());
        return undefined;
    }

    getAvailableExercises = () => {
        // vracamo samo kopije
        // return [ ...this.availableExercises ];
        return this.availableExercises.slice();
    }

    populateAvailableExercises = () => {
        let exercisesData = [
            { id: 'crunches', name: 'Crunches', duration: 30, calories: 8 },
            { id: 'touch-toes', name: 'Touch Toes', duration: 180, calories: 15 },
            { id: 'side-lunges', name: 'Side Lunges', duration: 120, calories: 18 },
            { id: 'burpees', name: 'Burpees', duration: 60, calories: 8 }
        ];

        exercisesData.forEach(exerciseData => {
            let exercise = this.exerciseFactory.create();
            exercise.setId(exerciseData.id);
            exercise.setName(exerciseData.name);
            exercise.setDuration(exerciseData.duration);
            exercise.setCalories(exerciseData.calories);
            this.availableExercises.push(exercise);
        });
    }

    notify = () => {
        this.observers.forEach(observer => {
            observer.update(this);
        });
    }

    getName = () => {
        return TRAINING_SERVICE;
    }
}