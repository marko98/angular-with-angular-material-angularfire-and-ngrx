import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

// interfaces
import { Request, AUTH_SERVICE } from '../models/patterns/behavioural/chain-of-responsibility/request.interface';

// models
import { Observable } from '../models/patterns/behavioural/observer/observable.model';
import { User } from '../models/user.model';
import { AuthData } from '../models/auth-data.model';

// factories
import { UserFactory } from '../models/patterns/creational/factories/user-factory';

// ovaj service sluzi samo da 'fake-ujemo' serversku 
// stranu za sada
@Injectable()
export class AuthService extends Observable implements Request {
    private user: User;
    private userFactory: UserFactory;

    constructor(private router: Router){
        super();
        this.userFactory = new UserFactory();
    }

    // ovde bi trebali da se salju zahtevi na server, za sada samo 'fake-ujemo'
    registerUser = (authData: AuthData) => {
        this.user = this.userFactory.create();
        this.user.setEmail(authData.getEmail());
        this.user.setUserId(Math.round(Math.random() * 10000).toString());
        this.onSuccessfulAuth();
    }

    // ovde bi trebali da se salju zahtevi na server, za sada samo 'fake-ujemo'
    login = (authData: AuthData) => {
        this.user = this.userFactory.create();
        this.user.setEmail(authData.getEmail());
        this.user.setUserId(Math.round(Math.random() * 10000).toString());
        this.onSuccessfulAuth();
    }

    // ovde bi trebali da se salju zahtevi na server, za sada samo 'fake-ujemo'
    logout = () => {
        this.user = null;
        this.notify();
        this.router.navigate(['auth', 'login']);
    }

    getUser = () => {
        if (this.user)
            return this.user.clone();
        return undefined;
    }

    isAuth = () => {
        return this.user != null;
    }

    onSuccessfulAuth = () => {
        this.notify();
        this.router.navigate(['trainings']);
    }

    notify = () => {
        this.observers.forEach(observer => {
            observer.update(this);
        });
    }

    getName = () => {
        return AUTH_SERVICE;
    }
}