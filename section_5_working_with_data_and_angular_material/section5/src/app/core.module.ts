import { NgModule } from '@angular/core';

// injectables
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './shared/models/auth.guard';
import { TrainingService } from './shared/services/training.service';

@NgModule({
    providers: [
        AuthService,
        AuthGuard,
        TrainingService
    ]
})
export class CoreModule{}