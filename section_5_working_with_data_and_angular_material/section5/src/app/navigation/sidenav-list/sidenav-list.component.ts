import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';

// services
import { AuthService } from '../../shared/services/auth.service';

// custom interfaces
import { Observer } from '../../shared/models/patterns/behavioural/observer/observer.interface';

// models
import { BaseHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { AuthServiceHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';

@Component({
    selector: 'app-sidenav-list',
    templateUrl: './sidenav-list.component.html',
    styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit, OnDestroy, Observer {
    @Output() closeSidenav = new EventEmitter<void>();
    public isAuth: boolean = false;
    private handler: BaseHandler;

    constructor(private authService: AuthService){
        this.authService.attach(this);
        this.handler = new AuthServiceHandler(this);
    }

    onLogout = () => {
        this.authService.logout();
        this.onCloseSidenav();
    }

    onCloseSidenav = () => {
        this.closeSidenav.emit();
    }

    update = (data?: any) => {
        if (data != null) {
            this.handler.handle(data);
        }
    }

    ngOnInit(){
        console.log("SidenavListComponent init");
    }

    ngOnDestroy(){
        if (this.authService)
            this.authService.dettach(this);
        console.log("SidenavListComponent destroyed");
    }
}