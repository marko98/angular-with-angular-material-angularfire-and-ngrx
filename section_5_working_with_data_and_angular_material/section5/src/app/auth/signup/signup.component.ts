import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

// services
import { AuthService } from '../../shared/services/auth.service';

// factories
import { AuthDataFactory } from '../../shared/models/patterns/creational/factories/auth-data-factory';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy{
    public maxDate: Date;
    private authDataFactory: AuthDataFactory;

    constructor(private authService: AuthService){
        this.authDataFactory = new AuthDataFactory();
    }

    onSubmit = (form: NgForm) => {
        let authData = this.authDataFactory.create();
        authData.setEmail(form.value.email);
        authData.setPassword(form.value.password);
        this.authService.registerUser(authData);
        // console.log(form);
    }

    ngOnInit(){
        this.maxDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        console.log("SignupComponent init");
    }

    ngOnDestroy(){
        console.log("SignupComponent destroyed");
    }

}