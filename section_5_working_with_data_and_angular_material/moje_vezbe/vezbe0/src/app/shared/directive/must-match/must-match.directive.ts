import { Directive, Input, OnInit, OnDestroy, HostListener, HostBinding } from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({
    selector: '[mustMatch]'
})
export class MustMatchDirective implements OnInit, OnDestroy{
    @Input() mustMatch: {original: NgModel, mustMatch: NgModel};

    // @HostListener('keyup') checkMatching() {
    //     if (this.mustMatch.original.value !== this.mustMatch.mustMatch.value){
    //         this.mustMatch.mustMatch.control.setErrors({'mustMatch': true});
    //         return
    //     }
    //     this.mustMatch.mustMatch.control.setErrors(null);
    // }

    @HostListener('change') checkMatching() {
        if (this.mustMatch.original.value !== this.mustMatch.mustMatch.value){
            this.mustMatch.mustMatch.control.setErrors({'mustMatch': true});
            return
        }
        this.mustMatch.mustMatch.control.setErrors(null);
    }

    ngOnInit(){
        console.log("MustMatchDirective init");
    }

    ngOnDestroy(){
        console.log("MustMatchDirective destroyed");
    }
}