import { Prototype } from './patterns/creational/prototype/prototype.interface';

export class Book implements Prototype {
    constructor(private name?: string, private description?: string, private imgPath?: string){}

    getName = () => {
        return this.name;
    }

    getDescription = () => {
        return this.description;
    }

    getImgPath = () => {
        return this.imgPath;
    }

    setName = (name: string) => {
        this.name = name;
    }

    setDescription = (description: string) => {
        this.description = description;
    }

    setImgPath = (imgPath: string) => {
        this.imgPath = imgPath;
    }

    protected constructorPrototype = (book: Book): void => {
        book.setName(this.getName());
        book.setDescription(this.getDescription());
        book.setImgPath(this.getImgPath());
    }

    clone = (): Prototype => {
        let clone = new Book();
        clone.setName(this.getName());
        clone.setDescription(this.getDescription());
        clone.setImgPath(this.getImgPath());
        return clone;
    }
}