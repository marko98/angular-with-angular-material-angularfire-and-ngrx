// interfaces
import { Prototype } from './patterns/creational/prototype/prototype.interface';

export class User implements Prototype {
    constructor(private email?: string, private userId?: string){}

    getEmail = () => {
        return this.email;
    }

    getUserId = () => {
        return this.userId;
    }

    setEmail = (email: string) => {
        this.email = email;
    }

    setUserId = (userId: string) => {
        this.userId = userId;
    }

    protected constructorPrototype = (user: User): void => {
        user.setEmail(this.getEmail());
        user.setUserId(this.getUserId());
    }

    clone = (): Prototype => {
        let clone = new User();
        clone.setEmail(this.getEmail());
        clone.setUserId(this.getUserId());
        return clone;
    }
}