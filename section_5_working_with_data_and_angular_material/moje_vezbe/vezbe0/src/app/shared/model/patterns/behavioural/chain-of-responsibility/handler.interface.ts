// interfaces
import { Request } from './request.interface';

// models
import { BaseHandler } from './base-handler.model';

export declare interface Handler {
    setNext(handler: BaseHandler): boolean;
    handle(request: Request): void;
}