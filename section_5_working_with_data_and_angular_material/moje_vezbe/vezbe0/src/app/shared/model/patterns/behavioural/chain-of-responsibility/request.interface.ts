// request names

// const names of services
export const AUTH_SERVICE = '[Auth] Auth Service';

// const names of dialogs
export const DIALOG_LOGOUT = '[Dialog] Dialog Logout';

export declare interface Request {
    getName(): string;
}