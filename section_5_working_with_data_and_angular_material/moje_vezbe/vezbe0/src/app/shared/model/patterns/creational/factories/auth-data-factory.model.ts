// models
import { AuthData } from '../../../auth-data.model';
import { Factory } from './factory';

export class AuthDataFactory extends Factory {
    create = () => {
        return new AuthData();
    }
}