import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

// models
import { LogoutDialog } from '../../model/logout-dialog.model';
import { Observable } from '../../model/patterns/behavioural/observer/observable.model';

@Component({
    selector: 'app-logout-dialog',
    templateUrl: './logout-dialog.component.html',
    styleUrls: ['./logout-dialog.component.css']
})
export class LogoutDialogComponent extends Observable implements OnInit, OnDestroy {
    public logoutDialog: LogoutDialog;

    constructor(private matDialogRef: MatDialogRef<LogoutDialogComponent>){
        super();
    }

    setModel = (logoutDialog: LogoutDialog) => {
        this.logoutDialog = logoutDialog;
    }

    onResult = (result: boolean) => {
        this.logoutDialog.result = result;
        this.notify();
        this.matDialogRef.close();
    }

    notify = () => {
        this.observes.forEach(observer => {
            observer.update(this.logoutDialog);
        });
    }

    ngOnInit(){
        console.log("LogoutDialogComponent init");
    }

    ngOnDestroy(){
        console.log("LogoutDialogComponent destroyed");
    }
}