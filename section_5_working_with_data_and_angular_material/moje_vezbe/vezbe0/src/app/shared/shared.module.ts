import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

// directives(dekorateri)
import { MustMatchDirective } from './directive/must-match/must-match.directive';

// components
import { LogoutDialogComponent } from './dialog/logout/logout-dialog.component';

@NgModule({
    declarations: [
        MustMatchDirective,
        LogoutDialogComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        FlexLayoutModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        FlexLayoutModule,

        MustMatchDirective
    ],
    entryComponents: [
        LogoutDialogComponent
    ]
})
export class SharedModule{}