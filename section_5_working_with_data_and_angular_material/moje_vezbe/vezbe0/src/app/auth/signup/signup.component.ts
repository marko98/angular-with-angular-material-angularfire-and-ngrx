import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

// models
import { AuthService } from '../../shared/service/auth.service';
import { AuthDataFactory } from '../../shared/model/patterns/creational/factories/auth-data-factory.model';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy{
    public showPassword: boolean = false;
    public maxAllowedDate: Date;
    private authDataFactory: AuthDataFactory;

    constructor(private authService: AuthService){}

    onSubmit = (form: NgForm) => {
        if (form.valid){
            // console.log(form);
            let authData = this.authDataFactory.create();
            authData.setEmail(form.value.email);
            authData.setPassword(form.value.password);
            this.authService.register(authData);
        }
    }

    onShowPassword = () => {
        this.showPassword = true;
        setTimeout(() => {
            this.showPassword = false;
        }, 1000);
    }

    ngOnInit(){
        this.authDataFactory = new AuthDataFactory();

        this.maxAllowedDate = new Date();
        this.maxAllowedDate.setFullYear(this.maxAllowedDate.getFullYear()-18);

        console.log("SignupComponent init");
    }

    ngOnDestroy(){
        console.log("SignupComponent destroyed");
    }

}