import { NgModule } from '@angular/core';

// services
import { BooksService } from './shared/service/books.service';
import { AuthService } from './shared/service/auth.service';

@NgModule({
    providers: [
        BooksService,
        AuthService
    ]
})
export class CoreModule {}