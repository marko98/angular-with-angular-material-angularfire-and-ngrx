import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { BooksComponent } from './books.component';
import { BookComponent } from './book/book.component';

@NgModule({
    declarations: [
        BooksComponent,
        BookComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        BooksComponent
    ]
})
export class BooksModule{}