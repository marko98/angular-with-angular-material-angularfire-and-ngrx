// modules, interfaces
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

// components

// custom classes, interfaces
import { Observer } from '../shared/model/patterns/behavioural/observer/observer.interface';

// models
import { Book } from '../shared/model/book.model';

// services
import { BooksService } from '../shared/service/books.service';

@Component({
    selector: 'app-books',
    templateUrl: './books.component.html',
    styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit, OnDestroy, Observer{
    public books: Book[] = [];
    public ownerName: string = "Marko";
    public hidePassword: boolean = true;

    constructor(private booksService: BooksService){}

    onAddBookSubmit = (form) => {
        console.log(form);
        if(form.valid){
            let imgPath = form.value.imgPath;
            if (imgPath === '' || imgPath === null){
                imgPath = "https://pngimage.net/wp-content/uploads/2018/06/peter-pan-png-1.png";
            }
            let description = form.value.description;
            if (description === '' || description === null){
                description = "no description";
            }
            // this.books.push(new Book(form.value.name, description, imgPath));
            this.booksService.addBook(new Book(form.value.name, description, imgPath));
            (<NgForm>form).resetForm();
        }
    }

    onShowPassword = () => {
        this.hidePassword = false;
        setTimeout(() => {
            this.hidePassword = true;
        }, 1000)
    }

    onRemove = (book) => {
        // this.books = this.books.filter(b => b !== book);
        this.booksService.removeBook(book);
    }

    update = () => {
        this.books = this.booksService.getBooks();
    }

    ngOnInit(){
        this.booksService.attach(<Observer>this);
        this.books = this.booksService.getBooks();
        console.log("BooksComponent init");
    }

    ngOnDestroy(){
        this.booksService.dettach(<Observer>this);
        console.log("BooksComponent destroyed");
    }
}