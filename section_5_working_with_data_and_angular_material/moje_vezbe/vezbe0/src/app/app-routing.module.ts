import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { BooksComponent } from './books/books.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'auth', children: [
        { path: 'login', component: LoginComponent },
        { path: 'signup', component: SignupComponent }
    ] },
    { path: 'books', component: BooksComponent},
    { path: '**', redirectTo: ''}
]
@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}