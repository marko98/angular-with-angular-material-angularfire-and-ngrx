import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

// models
import { BaseHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { AuthServiceHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';

// services
import { AuthService, AUTH_ACTIONS } from '../../shared/services/auth.service';

// factories
import { AuthDataFactory } from '../../shared/models/patterns/creational/factories/auth-data-factory';

// interfaces
import { Observer } from '../../shared/models/patterns/behavioural/observer/observer.interface';
import { AuthServiceHandlerInterface } from '../../shared/interfaces/auth-service-handler.interface';


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy, Observer, AuthServiceHandlerInterface {
    public maxDate: Date;
    private authDataFactory: AuthDataFactory;
    private handler: BaseHandler;
    public loading: boolean = false;

    constructor(private authService: AuthService){
        this.authDataFactory = new AuthDataFactory();
    }

    onSubmit = (form: NgForm) => {
        let authData = this.authDataFactory.create();
        authData.setEmail(form.value.email);
        authData.setPassword(form.value.password);
        this.authService.registerUser(authData);
        // console.log(form);
    }

    // implmentations of functions from interfaces

    update = (data?: any) => {
        if (data)
            this.handler.handle(data);
    }

    onAuthServiceUpdate = () => {
        this.loading = AUTH_ACTIONS.registrationInProgress;
    }

    ngOnInit(){
        this.handler = new AuthServiceHandler(this);
        this.authService.attach(this);

        this.maxDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        console.log("SignupComponent init");
    }

    ngOnDestroy(){
        if (this.authService)
            this.authService.dettach(this);

        console.log("SignupComponent destroyed");
    }

}