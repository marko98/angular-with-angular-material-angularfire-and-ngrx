import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// models
import { BaseHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { AuthServiceHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';

// services
import { AuthService, AUTH_ACTIONS } from '../../shared/services/auth.service';

// factories
import { AuthDataFactory } from '../../shared/models/patterns/creational/factories/auth-data-factory';

// interfaces
import { Observer } from '../../shared/models/patterns/behavioural/observer/observer.interface';
import { AuthServiceHandlerInterface } from '../../shared/interfaces/auth-service-handler.interface';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy, Observer, AuthServiceHandlerInterface {
    public loginForm: FormGroup;
    public showPassword: boolean = false;
    private authDataFactory: AuthDataFactory;
    private handler: BaseHandler;
    public loading: boolean = false;

    constructor(private authService: AuthService){
        this.authDataFactory = new AuthDataFactory();
    }

    onSubmit = () => {
        let authData = this.authDataFactory.create();
        authData.setEmail(this.loginForm.value.email);
        authData.setPassword(this.loginForm.value.password);
        this.authService.login(authData);
        // console.log(this.loginForm);
    }

    onShowPassword = () => {
        this.showPassword = true;

        setTimeout(() => {
            this.showPassword = false;
        }, 1000);
    }

    // implmentations of functions from interfaces

    update = (data?: any) => {
        if (data)
            this.handler.handle(data);
    }

    onAuthServiceUpdate = () => {
        this.loading = AUTH_ACTIONS.loginInProgress;
    }

    ngOnInit(){
        this.handler = new AuthServiceHandler(this);
        this.authService.attach(this);

        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email], []),
            password: new FormControl('', [Validators.required, Validators.minLength(6)], [])
        });
        console.log("LoginComponent init");
    }

    ngOnDestroy(){
        if (this.authService)
            this.authService.dettach(this);
        console.log("LoginComponent destroyed");
    }
}