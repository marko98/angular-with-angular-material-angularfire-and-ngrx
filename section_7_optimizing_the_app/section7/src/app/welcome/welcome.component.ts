import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnDestroy {
    ngOnInit(){
        console.log("WelcomeComponent init");
    }

    ngOnDestroy(){
        console.log("WelcomeComponent destroyed");
    }
}