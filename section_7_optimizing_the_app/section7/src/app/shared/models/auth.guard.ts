import { Injectable } from '@angular/core';
import { 
    CanActivate, 
    ActivatedRouteSnapshot, 
    RouterStateSnapshot, 
    Router, 
    CanLoad,
    Route
} from '@angular/router';

// services
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
    constructor(private authService: AuthService, private router: Router){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authService.getIsAuth()) { 
            return true;
        }
        return this.router.createUrlTree(['auth', 'login']);
    }

    canLoad(route: Route){
        if (this.authService.getIsAuth()) { 
            return true;
        }
        this.router.navigate(['auth', 'login']);
    }
}