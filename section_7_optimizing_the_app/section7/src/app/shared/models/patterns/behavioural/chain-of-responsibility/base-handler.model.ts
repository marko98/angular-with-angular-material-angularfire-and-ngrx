// custom interface
import { Handler } from './handler.interface';

export class BaseHandler implements Handler {
    protected next: BaseHandler;

    setNext = (handler: BaseHandler) => {
        this.next = handler;
        return true;
    }

    handle = (request: any) => {
        console.log("handle isn't implemented");
    }
}