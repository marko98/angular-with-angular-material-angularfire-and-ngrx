import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

// components
import { StopTrainingDialogComponent } from './dialogs/stop-training/stop-training-dialog.component';

@NgModule({
    declarations: [
        StopTrainingDialogComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        FlexLayoutModule,
        AngularFirestoreModule,
        AngularFireAuthModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        FlexLayoutModule,
        AngularFirestoreModule,
        AngularFireAuthModule
    ],
    // buduci da ce se programatski :) dodavati
    entryComponents: [
        StopTrainingDialogComponent
    ]
})
export class SharedModule{}