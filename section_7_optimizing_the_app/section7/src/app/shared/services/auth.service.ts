import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

// interfaces
import { Request, AUTH_SERVICE } from '../models/patterns/behavioural/chain-of-responsibility/request.interface';

// models
import { Observable } from '../models/patterns/behavioural/observer/observable.model';
import { AuthData } from '../models/auth-data.model';

// services
import { UIService } from './ui.service';

export const AUTH_ACTIONS = {
    loginInProgress: false,
    registrationInProgress: false
};

// ovaj service sluzi samo da 'fake-ujemo' serversku 
// stranu za sada
@Injectable()
export class AuthService extends Observable implements Request {
    private isAuth: boolean = false;

    constructor(private router: Router,
                private angularFireAuth: AngularFireAuth,
                private uIService: UIService){
        super();
        this.autoLoginLogout();
    }

    registerUser = (authData: AuthData) => {
        AUTH_ACTIONS.registrationInProgress = true;
        this.angularFireAuth.
            auth.
            createUserWithEmailAndPassword(authData.getEmail(), authData.getPassword())
            .then(result => {
                // console.log(result);
            })
            .catch(err => {
                this.uIService.showSnackbar(err.message, null, 3000);
                // console.log(err);
            }).finally(() => {
                AUTH_ACTIONS.registrationInProgress = false;
                this.notify();
            });

        this.notify();
    }

    private autoLoginLogout = () => {
        this.angularFireAuth
            .authState
            .subscribe(
                user => {
                    if (user) {
                        this.isAuth = true;
                        this.notify();
                        this.router.navigate(['trainings']);
                    } else {
                        this.isAuth = false;
                        this.notify();
                        this.router.navigate(['auth', 'login']);
                    }
                },
                err => {
                    console.log(err);
                }
            );
    }

    login = (authData: AuthData) => {
        AUTH_ACTIONS.loginInProgress = true;

        this.angularFireAuth
            .auth
            .signInWithEmailAndPassword(authData.getEmail(), authData.getPassword())
            .then(result => {
                // console.log(result);
            })
            .catch(err => {
                this.uIService.showSnackbar(err.message, null, 3000);
                // console.log(err);
            })
            .finally(() => {
                AUTH_ACTIONS.loginInProgress = false;
                this.notify();
            });

        this.notify();
    }

    logout = () => {
        this.angularFireAuth.auth.signOut();
    }

    getIsAuth = () => {
        return this.isAuth;
    }

    notify = () => {
        this.observers.forEach(observer => {
            observer.update(this);
        });
    }

    getName = () => {
        return AUTH_SERVICE;
    }
}