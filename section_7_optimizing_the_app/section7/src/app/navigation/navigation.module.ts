import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

// components
import { HeaderComponent } from './header/header.component';
import { SidenavListComponent } from './sidenav-list/sidenav-list.component';

@NgModule({
    declarations: [
        HeaderComponent,
        SidenavListComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        HeaderComponent,
        SidenavListComponent
    ]
})
export class NavigationModule{}