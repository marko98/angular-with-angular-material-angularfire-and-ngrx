import { Component, Input, OnInit, OnDestroy } from '@angular/core';

// services
import { AuthService } from '../../shared/services/auth.service';

// custom interfaces
import { Observer } from '../../shared/models/patterns/behavioural/observer/observer.interface';
import { AuthServiceHandlerInterface } from '../../shared/interfaces/auth-service-handler.interface';

// models
import { BaseHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { AuthServiceHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy, Observer, AuthServiceHandlerInterface {
    @Input() sidenav;
    public isAuth: boolean = false;
    private handler: BaseHandler;

    constructor(private authService: AuthService){
        this.authService.attach(this);
        this.handler = new AuthServiceHandler(this);
    }

    onLogout = () => {
        this.authService.logout();
    }

    update = (data?: any) => {
        if (data != null){
            this.handler.handle(data);
        }
    }

    onAuthServiceUpdate = () => {
        this.isAuth = this.authService.getIsAuth();
    }

    ngOnInit(){
        console.log("HeaderComponent init");
    }

    ngOnDestroy(){
        if (this.authService)
            this.authService.dettach(this);
        console.log("HeaderComponent destroyed");
    }
}