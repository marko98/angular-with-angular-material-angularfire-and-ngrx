import { NgModule } from '@angular/core';

// injectables
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './shared/models/auth.guard';
import { TrainingService } from './shared/services/training.service';
import { UIService } from './shared/services/ui.service';

@NgModule({
    providers: [
        AuthService,
        AuthGuard,
        TrainingService,
        UIService
    ]
})
export class CoreModule{}