import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { WelcomeComponent } from './welcome/welcome.component';

// models
import { AuthGuard } from './shared/models/auth.guard';

const routes: Routes = [
    { path: '', component: WelcomeComponent },
    // lazy loading
    { path: 'trainings', loadChildren: () => import('./training/training.module').then(m => m.TrainingModule), canLoad: [AuthGuard]},
    // { path: 'trainings', loadChildren: './training/training.module#TrainingModule', canLoad: [AuthGuard]},
    { path: '**', redirectTo: '' }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        
    ]
})
export class AppRoutingModule{}