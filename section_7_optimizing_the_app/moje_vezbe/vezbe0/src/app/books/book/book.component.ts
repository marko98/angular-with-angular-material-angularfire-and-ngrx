import { Component, Input, Output, EventEmitter, HostListener, OnInit, OnDestroy } from '@angular/core';

import { Book } from 'src/app/shared/model/book.model';

@Component({
    selector: 'app-book',
    templateUrl: './book.component.html',
    styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit, OnDestroy{
    @Input() book: Book;
    @Output() onRemoveBook = new EventEmitter();

    onRemove = () => {
        this.onRemoveBook.emit();
    }

    @HostListener("click") onClick = () => {
        console.log("BookComponent click event happened")
    }

    @HostListener("copy") onCopy = () => {
        console.log("BookComponent copy event happened")
    }

    ngOnInit(){
        console.log("BookComponent init");
    }

    ngOnDestroy(){
        console.log("BookComponent destroyed");
    }
}