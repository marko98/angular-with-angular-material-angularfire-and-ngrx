// interfaces
import { Request, DIALOG_LOGOUT } from '../model/patterns/behavioural/chain-of-responsibility/request.interface';

export class LogoutDialog implements Request {
    public result: boolean;

    constructor(public name?: string){}

    getName = () => {
        return DIALOG_LOGOUT;
    }
}