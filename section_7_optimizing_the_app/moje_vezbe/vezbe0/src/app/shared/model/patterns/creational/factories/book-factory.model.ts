// models
import { Factory } from './factory';
import { Book } from '../../../book.model';

export class BookFactory extends Factory{
    create = () => {
        return new Book();
    }
}