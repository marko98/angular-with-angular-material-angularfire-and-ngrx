export abstract class Factory {
    abstract create(): any;
}