// interfaces
import { Handler } from './handler.interface';
import { Request } from './request.interface';

export abstract class BaseHandler implements Handler {
    protected next: BaseHandler;

    setNext = (handler: BaseHandler) => {
        if (handler !== undefined) {
            this.next = handler;
            return true;
        }
        return false;
    }

    handle = (request: Request) => {
        console.log("handle isn't implemented");
    }

}