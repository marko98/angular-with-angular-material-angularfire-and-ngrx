import { Observer } from './observer.interface';

export abstract class Observable{
    protected observes: Observer[] = [];

    // abstract attach(observer: Observer): void;
    attach = (observer: Observer) => {
        this.observes.push(observer);
    }

    dettach = (observer: Observer) => {
        this.observes = this.observes.filter(o => o !== observer);
    }

    notify = () => {
        this.observes.forEach(observer => {
            observer.update();
        });
    }
}