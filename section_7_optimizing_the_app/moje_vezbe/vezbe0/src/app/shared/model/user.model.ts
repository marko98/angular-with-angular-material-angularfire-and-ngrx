// interfaces
import { Prototype } from './patterns/creational/prototype/prototype.interface';

export class User implements Prototype {
    constructor(private email?: string, private userId?: string){}

    getEmail = () => {
        return this.email;
    }

    getUserId = () => {
        return this.userId;
    }

    setEmail = (email: string) => {
        this.email = email;
    }

    setUserId = (userId: string) => {
        this.userId = userId;
    }

    clone = () => {
        return { ...this };
    }
}