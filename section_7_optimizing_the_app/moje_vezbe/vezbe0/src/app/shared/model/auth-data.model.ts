export class AuthData {
    private email: string;
    private password: string;

    getEmail = () => {
        return this.email;
    }

    getPassword = () => {
        return this.password;
    }

    setEmail = (email: string) => {
        this.email = email;
    }

    setPassword = (password: string) => {
        this.password = password;
    }
}