// models
import { Book } from '../model/book.model';
import { Observable } from '../model/patterns/behavioural/observer/observable.model';

export class BooksService extends Observable  {
    private books: Book[] = [];

    addBook = (book: Book) => {
        this.books.push(book);
        this.notify();
    }

    removeBook = (book: Book) => {
        this.books = this.books.filter(b => b !== book);
        this.notify();
    }

    getBooks = () => {
        // ne zelimo da vratimo referencu vec samo kopiju
        return [...this.books];
    }
}