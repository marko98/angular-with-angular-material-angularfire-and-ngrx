import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

// models
import { AuthService } from '../../shared/service/auth.service';
import { AuthServiceHandler } from '../../shared/model/patterns/behavioural/chain-of-responsibility/auth-service-handler.model';
import { LogoutDialogHandler } from '../../shared/model/patterns/behavioural/chain-of-responsibility/logout-dialog-handler.model';
import { LogoutDialog } from 'src/app/shared/model/logout-dialog.model';

// interfaces
import { Observer } from '../../shared/model/patterns/behavioural/observer/observer.interface';

// components
import { LogoutDialogComponent } from '../../shared/dialog/logout/logout-dialog.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy, Observer {
    @Input() sidenav;
    private authServiceHandler: AuthServiceHandler;
    private matDialogRef: MatDialogRef<LogoutDialogComponent>;
    private logoutDialogComponent: LogoutDialogComponent;
    public isAuth: boolean = false;

    constructor(private authService: AuthService,
                private matDialog: MatDialog){
        this.authService.attach(this);
    }

    onLogout = () => {
        this.matDialogRef = this.matDialog.open(LogoutDialogComponent);
        this.logoutDialogComponent = this.matDialogRef.componentInstance;
        this.logoutDialogComponent.attach(this);
        this.logoutDialogComponent.setModel(new LogoutDialog("Marko"));
    }

    update (request?: any) {
        if (request !== undefined){
            this.authServiceHandler.handle(request);
        }
    }

    ngOnInit(){
        this.authServiceHandler = new AuthServiceHandler(this);
        let logoutDialogHandler = new LogoutDialogHandler(this.authService);
        this.authServiceHandler.setNext(logoutDialogHandler);
        console.log("HeaderComponent init");
    }

    ngOnDestroy(){
        if (this.logoutDialogComponent !== undefined)
            this.logoutDialogComponent.dettach(this);
        this.authService.dettach(this);
        console.log("HeaderComponent destroyed");
    }
}