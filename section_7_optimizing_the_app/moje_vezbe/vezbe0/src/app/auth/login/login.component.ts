import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// models
import { AuthService } from '../../shared/service/auth.service';
import { AuthDataFactory } from '../../shared/model/patterns/creational/factories/auth-data-factory.model';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    public showPassword: boolean = false;
    public loginForm: FormGroup;
    private authDataFactory: AuthDataFactory;

    constructor(private authService: AuthService){}

    onShowPassword = () => {
        this.showPassword = true;

        setTimeout(() => {
            this.showPassword = false;
        }, 1000);
    }

    onSubmit = () => {
        if (this.loginForm.valid){
            // console.log(this.loginForm);
            let authData = this.authDataFactory.create();
            authData.setEmail(this.loginForm.value.email);
            authData.setPassword(this.loginForm.value.password);
            this.authService.register(authData);
        }
    }

    ngOnInit(){
        this.authDataFactory = new AuthDataFactory();

        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.email, Validators.required]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)])
        });

        console.log("LoginComponent init");
    }

    ngOnDestroy(){
        console.log("LoginComponent destroyed");
    }

}