import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    public loginForm: FormGroup;
    public showPassword: boolean = false;

    onSubmit = () => {
        console.log(this.loginForm);
    }

    onShowPassword = () => {
        this.showPassword = true;

        setTimeout(() => {
            this.showPassword = false;
        }, 1000);
    }

    ngOnInit(){
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email], []),
            password: new FormControl('', [Validators.required, Validators.minLength(6)], [])
        });
        console.log("LoginComponent init");
    }

    ngOnDestroy(){
        console.log("LoginComponent destroyed");
    }
}