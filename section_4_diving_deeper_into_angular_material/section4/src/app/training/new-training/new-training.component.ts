import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-new-training',
    templateUrl: './new-training.component.html',
    styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit, OnDestroy{
    @Output() startTraining = new EventEmitter<void>();

    onStartTraining = () => {
        this.startTraining.emit();
    }

    ngOnInit(){
        console.log("NewTrainingComponent init");
    }

    ngOnDestroy(){
        console.log("NewTrainingComponent destroyed");
    }
}