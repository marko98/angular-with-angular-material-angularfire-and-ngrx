import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

// components
import { StopTrainingDialogComponent } from '../../shared/dialogs/stop-training/stop-training-dialog.component';

// custom interfaces
import { Observer } from '../../shared/models/observer.interface';

@Component({
    selector: 'app-current-training',
    templateUrl: './current-training.component.html',
    styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit, OnDestroy, Observer{
    @Output() trainingOver = new EventEmitter<void>();
    public progress: number = 0;
    public timer: number;
    private stopTrainingDialogComponent: StopTrainingDialogComponent;

    // MatDialog je servis koji nam sluzi da bismo instancirali komponentu u run-time-u
    // wrap-uje nasu komponentu i ona dobija jos neke osobine (kao dekorater) 
    constructor(private dialog: MatDialog){}

    onStop = () => {
        clearInterval(this.timer);
        // prosledjujemo nasu komponentu koja mora biti unutar:
        // entryComponents: [
        //        StopTrainingDialogComponent
        //    ]
        // u modulu koji je 'export-uje'
        let dialogRef = this.dialog.open(StopTrainingDialogComponent, {
            width: '250px'
        });
        this.stopTrainingDialogComponent = (<StopTrainingDialogComponent>dialogRef.componentInstance);
        this.stopTrainingDialogComponent.stopTrainingDialogData.attach(this);

        this.stopTrainingDialogComponent.setModelData({name: "Marko", progress: this.progress});
    }

    update = () => {
        if (!(this.stopTrainingDialogComponent.stopTrainingDialogData.stopTraining)){
            this.startOrResumeTimer();
        } else {
            this.trainingOver.emit();
        }

        // console.log((<StopTrainingDialogComponent>this.dialogRef.componentInstance).stopTrainingDialogData.stopTraining);
    }

    private startOrResumeTimer = () => {
        this.timer = setInterval(() => {
            this.progress += 5;

            if (this.progress >= 100)
                clearInterval(this.timer);

        }, 1000);
    }

    ngOnInit(){
        this.startOrResumeTimer();
        console.log("CurrentTrainingComponent init");
    }

    ngOnDestroy(){
        if (this.stopTrainingDialogComponent)
            this.stopTrainingDialogComponent.stopTrainingDialogData.dettach(this);
        console.log("CurrentTrainingComponent destroyed");
    }

}