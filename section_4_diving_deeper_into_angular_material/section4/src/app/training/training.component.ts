import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-training',
    templateUrl: './training.component.html',
    styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit, OnDestroy {
    ongoingTraining: boolean = false;

    onStartTraining = () => {
        this.ongoingTraining = true;
    }
    
    ngOnInit(){
        console.log("TrainingComponent init");
    }

    ngOnDestroy(){
        console.log("TrainingComponent destroyed");
    }
}