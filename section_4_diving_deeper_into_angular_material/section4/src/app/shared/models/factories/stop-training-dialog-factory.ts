import { StopTrainingDialog } from '../stop-training-dialog.model';

export class StopTrainingDialogFactory {
    create = () => {
        return new StopTrainingDialog();
    }
}