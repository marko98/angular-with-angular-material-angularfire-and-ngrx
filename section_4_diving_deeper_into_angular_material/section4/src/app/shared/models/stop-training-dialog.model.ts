import { Observable } from './observable.model';

export class StopTrainingDialog extends Observable {
    public stopTraining: boolean;
    public name: string;
    public progress: number;
}