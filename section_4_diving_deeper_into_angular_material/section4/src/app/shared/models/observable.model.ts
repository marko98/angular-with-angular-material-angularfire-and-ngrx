import { Observer } from './observer.interface';

export abstract class Observable {
    private observers: Observer[] = [];

    // abstract attach(observer: Observer);
    attach = (observer: Observer) => {
        this.observers.push(observer);
    }

    dettach = (observer: Observer) => {
        this.observers = this.observers.filter(o => o !== observer);
    }

    notify = () => {
        this.observers.forEach(observer => {
            observer.update();
        });
    }
}