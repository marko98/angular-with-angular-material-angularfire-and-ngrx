import { Component, OnInit, OnDestroy } from '@angular/core';

import { MatDialogRef } from '@angular/material/dialog';

// custom classes
import { StopTrainingDialog } from '../../models/stop-training-dialog.model';
import { StopTrainingDialogFactory } from '../../models/factories/stop-training-dialog-factory';

@Component({
    selector: 'app-stop-training-dialog',
    templateUrl: './stop-training-dialog.component.html',
    styleUrls: ['./stop-training-dialog.component.css']
})
export class StopTrainingDialogComponent implements OnInit, OnDestroy {
    public stopTrainingDialogData: StopTrainingDialog = null;

    constructor(public dialogRef: MatDialogRef<StopTrainingDialogComponent>){
        let factory = new StopTrainingDialogFactory();
        this.stopTrainingDialogData = factory.create();
    }

    setModelData = (data: { name? : string, progress? : number}) => {
        this.stopTrainingDialogData.progress = data.progress;
        this.stopTrainingDialogData.name = data.name;
        return true;
    }

    onResultSet = (result: boolean) => {
        this.stopTrainingDialogData.stopTraining = result;
        this.dialogRef.close();
        this.stopTrainingDialogData.notify();
    }

    ngOnInit(){
        console.log("StopTrainingDialogComponent init");
    }

    ngOnDestroy(){
        console.log("StopTrainingDialogComponent destroyed");
    }
}