import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-sidenav-list',
    templateUrl: './sidenav-list.component.html',
    styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit, OnDestroy{
    @Output() closeSidenav = new EventEmitter<void>();

    onCloseSidenav = () => {
        this.closeSidenav.emit();
    }

    ngOnInit(){
        console.log("SidenavListComponent init");
    }

    ngOnDestroy(){
        console.log("SidenavListComponent destroyed");
    }
}