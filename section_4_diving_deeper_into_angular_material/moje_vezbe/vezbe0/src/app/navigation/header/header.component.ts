import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy{
    @Input() sidenav;

    ngOnInit(){
        console.log("HeaderComponent init");
    }

    ngOnDestroy(){
        console.log("HeaderComponent destroyed");
    }
}