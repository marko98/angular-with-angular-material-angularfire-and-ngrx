import { NgModule } from '@angular/core';

import { BooksService } from './books/books.service';

@NgModule({
    providers: [
        BooksService
    ]
})
export class CoreModule {}