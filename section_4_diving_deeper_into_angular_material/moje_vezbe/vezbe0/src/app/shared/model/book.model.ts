export class Book{
    constructor(private name?: string, private description?: string, private imgPath?: string){}

    getName = () => {
        return this.name;
    }

    getDescription = () => {
        return this.description;
    }

    getImgPath = () => {
        return this.imgPath;
    }

    setName = (name: string) => {
        this.name = name;
    }

    setDescription = (description: string) => {
        this.description = description;
    }

    setImgPath = (imgPath: string) => {
        this.imgPath = imgPath;
    }
}