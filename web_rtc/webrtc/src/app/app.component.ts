import { Component, OnInit, OnDestroy } from '@angular/core';

// services
import { WebRTCService } from './shared/service/webrtc.service';

// interfaces
import { Observer } from './shared/model/patterns/behavioural/observer/observer.interface';

/*
for WebRTC check:
https://webrtc.org/getting-started
*/

const constraints = {
  video: true,
  audio: true
}

// const constraints = {
//   'audio': {'echoCancellation': true},
//   'video': {
//     'deviceId': cameraId,
//     'width': {'min': minWidth},
//     'height': {'min': minHeight}
//     }
// }

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy, Observer {
  title = 'webrtc';
  private stream: MediaStream;

  constructor(private webRTCService: WebRTCService){}

  onPlayVideo = (videoElement: HTMLVideoElement) => {
    this.webRTCService
      .getInputOutputMediaStream(constraints)
      .then(stream => {
        this.stream = stream;
        console.log(this.stream);
        videoElement.srcObject = this.stream;
      })
      .catch(err => {
        console.log(err);
      });
  }

  // A MediaStream represents a stream of media content, 
  // which consists of tracks (MediaStreamTrack) of audio and video.
  // get media stream tracks
  onGetMediaStreamTracks = () => {
    let mediaTracks: MediaStreamTrack[];
    if (this.stream){
      mediaTracks = this.stream.getTracks();
      console.log(mediaTracks);
    }
  }

  onToggleVideoOrAudio = (kind: "audio" | "video") => {
    if (this.stream) {
      this.stream.getTracks().forEach(mediaTrack => {
        if (mediaTrack.kind === kind)
          mediaTrack.enabled = !mediaTrack.enabled;
      });
    }
  }

  onQueryMediaDevices = () => {
    // querying media devices
    this.webRTCService.getConnectedDevices('videoinput', cameras => console.log("Cameras(videoinput) found: ", cameras));
    this.webRTCService.getConnectedDevices('audioinput', audios => console.log("Audios(audioinput) found: ", audios));
    this.webRTCService.getConnectedDevices('audiooutput ', audios => console.log("Audios(audiooutput) found: ", audios));
  }

  update = (data?: any) => {
    console.log("devicechange event happened");
  }
 
  ngOnInit(){
    this.webRTCService.attach(this);
    console.log("AppComponent init");
  }

  ngOnDestroy(){
      if (this.webRTCService)
        this.webRTCService.dettach(this);
      console.log("AppComponent destroyed");
  }
}