import { Injectable } from "@angular/core";

// models
import { Observable } from '../model/patterns/behavioural/observer/observable.model';

@Injectable()
export class WebRTCService extends Observable {

    constructor(){
        super();

        // listenings for device changes
        navigator
            .mediaDevices
            .addEventListener('devicechange', event => {
                // const newCameraList = this.getConnectedDevices('video');
                // updateCameraList(newCameraList);
                // ...
                this.notify();
            });
    }

    // getting media stream
    getInputOutputMediaStream = (constraints: any) => {
        return navigator
            .mediaDevices
            .getUserMedia(constraints);
    }

    getConnectedDevices = (type, callback) => {
        navigator
          .mediaDevices
          .enumerateDevices()
          .then(mediaDevicesInfo => {
            const theMediaDeviceInfo = mediaDevicesInfo.filter(mediaDeviceInfo => mediaDeviceInfo.kind === type);
            callback(theMediaDeviceInfo);
          })
          .catch(err => {
            console.log(err);
          })
      }
}