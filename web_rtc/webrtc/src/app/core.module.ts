import { NgModule } from '@angular/core';

// services
import { WebRTCService } from './shared/service/webrtc.service';

@NgModule({
    providers: [
        WebRTCService
    ]
})
export class CoreModule{}