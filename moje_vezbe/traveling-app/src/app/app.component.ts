import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

// services
import { DeviceDetectorService } from 'ngx-device-detector';

export const device = {
  isMobile: false,
  isTablet: false,
  isDesktopDevice: false
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'traveling-app';
  public iconFindName: string = "keyboard_arrow_right";

  constructor(private deviceDetectorService: DeviceDetectorService){}

  private resolveDevice = () => {
    // console.log(this.deviceDetectorService.getDeviceInfo());
    device.isMobile = this.deviceDetectorService.isMobile();
    device.isTablet = this.deviceDetectorService.isTablet();
    device.isDesktopDevice = this.deviceDetectorService.isDesktop();
    // console.log(device);
  }

  onMenuClosed = () => {
    this.iconFindName = "keyboard_arrow_right";
  }

  onMenuOpened = () => {
    this.iconFindName = "keyboard_arrow_left";
  }

  ngOnInit(): void {
    this.resolveDevice();
      console.log("AppComponent init");
  }

  ngOnDestroy(): void {
      console.log("AppComponent destroyed");
  }
}
