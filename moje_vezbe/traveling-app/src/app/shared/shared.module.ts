import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DeviceDetectorModule } from 'ngx-device-detector';

const sharedModules = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule
]

@NgModule({
    imports: [...sharedModules, DeviceDetectorModule.forRoot()],
    exports: [...sharedModules, DeviceDetectorModule]
})
export class SharedModule {}