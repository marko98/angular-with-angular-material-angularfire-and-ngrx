import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';

// components
import { WelcomeComponent } from './welcome/welcome.component';

// vrednosti moraju biti tipa Route
export const ROUTES = {
    WELCOME_COMPONENT_ROUTE: { path: '', component: WelcomeComponent },
    UNKNOWN_ROUTE: { path: '**', redirectTo: '' }
}

const routes: Routes = [
    ROUTES.WELCOME_COMPONENT_ROUTE,
    ROUTES.UNKNOWN_ROUTE
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}