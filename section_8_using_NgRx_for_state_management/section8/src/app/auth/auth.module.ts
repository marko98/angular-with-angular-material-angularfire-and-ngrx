import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';

// components
import { LoginComponent } from './login/login.component';
import { SignupComponent} from './signup/signup.component';

@NgModule({
    declarations: [
        LoginComponent,
        SignupComponent
    ],
    imports: [
        SharedModule,
        AuthRoutingModule
    ],
    exports: [
        
    ]
})
export class AuthModule{}