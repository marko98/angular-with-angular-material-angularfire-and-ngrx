import { Component, OnInit, OnDestroy} from '@angular/core';
import { NgForm } from '@angular/forms';

// services
import { TrainingService, TRAINING_ACTIONS } from '../../shared/services/training.service';

// models
import { Exercise  } from '../../shared/models/exercise.model';
import { BaseHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/base-handler.model';
import { TrainingServiceHandler } from '../../shared/models/patterns/behavioural/chain-of-responsibility/training-service-handler';

// interfaces
import { Observer } from '../../shared/models/patterns/behavioural/observer/observer.interface';
import { TrainingServiceHandlerInterface } from '../../shared/interfaces/training-service-handler.interface';

@Component({
    selector: 'app-new-training',
    templateUrl: './new-training.component.html',
    styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit, OnDestroy, 
                                            Observer, TrainingServiceHandlerInterface {
    public exercises: Exercise[] = [];
    private handler: BaseHandler;
    public loading: boolean = true;

    constructor(private trainingService: TrainingService){}

    onSubmit = (form: NgForm): void => {
        if (form.valid) {
            // console.log(form);
            this.trainingService.setTrainingInProgress(form.value.selectedExercise);
        }
    }

    onFetchAgain = (): void => {
        this.trainingService.fetchAvailableExercises();
    }

    // interface methods

    update = (data?: any): void => {
        if (data)
            this.handler.handle(data);
    }

    onTrainingServiceUpdate = (): void => {
        // sada znamo da se data iz metode update ticala Training Service-a
        this.loading = TRAINING_ACTIONS.fetchingAvailableExercisesInProgress;
        this.exercises = this.trainingService.getAvailableExercises();
    }

    ngOnInit(){
        this.handler = new TrainingServiceHandler(this);
        this.trainingService.attach(this);

        console.log("NewTrainingComponent init");
    }

    ngOnDestroy(){
        if (this.trainingService)
            this.trainingService.dettach(this);
        console.log("NewTrainingComponent destroyed");
    }
}