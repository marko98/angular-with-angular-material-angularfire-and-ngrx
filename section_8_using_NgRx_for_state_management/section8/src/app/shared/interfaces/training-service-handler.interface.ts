export declare interface TrainingServiceHandlerInterface {
    onTrainingServiceUpdate(): void;
}