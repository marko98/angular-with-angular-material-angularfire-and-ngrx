// request names

// const names of services
export const AUTH_SERVICE = '[Auth] Auth Service';
export const TRAINING_SERVICE = '[Training] Training Service';

export declare interface Request {
    getName(): string;
}