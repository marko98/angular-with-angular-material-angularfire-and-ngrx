// models
import { AuthData } from '../../../auth-data.model';

// interfaces
import { Factory } from './factory.interface';

export class AuthDataFactory implements Factory {
    create = () => {
        return new AuthData();
    }
}